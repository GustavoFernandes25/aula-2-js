let dict = [ // Dicionario contendo os alunos, adicionei mais alunos para impedir possiveis erros.
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Jubileu",
        "turma": "A",
        "nota1": 8,
        "nota2": 10
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Juliete",
        "turma": "B",
        "nota1": 6,
        "nota2": 8
    },
    {
        "nome": "Jonathan",
        "turma": "C",
        "nota1": 6,
        "nota2": 9
    },
    {
        "nome": "Macabeu",
        "turma": "C",
        "nota1": 10,
        "nota2": 7
    }
]

// Aqui é um objeto/dicionário onde é adicionado o nome e a nota do aluno que possui a maior média da turma.
totMediaAlunos = {
    "TurmaA": {"nome":"", "media":0},
    "TurmaB": {"nome":"", "media":0},
    "TurmaC": {"nome":"", "media":0}
}
// função que retorna a média de um aluno
function media(a, b) {
    let mediaFim = 0
    mediaFim = (a + b) /2
    return mediaFim
}

//Laço que vai passar pelos alunos informados.

for (i in dict){

    //TURMA A
    if (dict[i]['turma'] == 'A'){ //Aqui é observado apenas os alunos da turma A
        let mediaAluno = media(dict[i]['nota1'], dict[i]['nota2']) // É obtida da media do primeiro aluno da turma A
        
        if (mediaAluno > totMediaAlunos["TurmaA"]["media"]){ // Aqui é verificado se já possui uma media cadastrada na let totMediaAlunos["TurmaA"], se não tiver ele cadastra a media obtida e se já estiver uma ele compara para ver qual é a maior e se for o caso mudar.
            totMediaAlunos["TurmaA"]["nome"] = dict[i]['nome']//Adiciona o nome do aluno
            totMediaAlunos["TurmaA"]["media"] = mediaAluno//Adiciona a nota do Aluno
        }

    }

    //TURMA B
    if (dict[i]['turma'] == 'B'){ //Aqui é observado apenas os alunos da turma B
        let mediaAluno = media(dict[i]['nota1'], dict[i]['nota2']) // É obtida da media do primeiro aluno da turma B

        if (mediaAluno > totMediaAlunos["TurmaB"]["media"]){ // Aqui é verificado se já possui uma media cadastrada na let totMediaAlunos["TurmaB"], se não tiver ele cadastra a media obtida e se já estiver uma ele compara para ver qual é a maior e se for o caso mudar.
            totMediaAlunos["TurmaB"]["nome"] = dict[i]['nome'] //Adiciona o nome do aluno
            totMediaAlunos["TurmaB"]["media"] = mediaAluno //Adiciona a nota do aluno
        }
    }

    //TURMA C
    if (dict[i]['turma'] == 'C'){ //Aqui é observado apenas os alunos da turma C
        let mediaAluno = media(dict[i]['nota1'], dict[i]['nota2']) // É obtida da media do primeiro aluno da turma C

        if (mediaAluno > totMediaAlunos["TurmaC"]["media"]){ // Aqui é verificado se já possui uma media cadastrada na let totMediaAlunos["TurmaC"], se não tiver ele cadastra a media obtida e se já estiver uma ele compara para ver qual é a maior e se for o caso mudar.
            totMediaAlunos["TurmaC"]["nome"] = dict[i]['nome']//Adiciona o nome do aluno
            totMediaAlunos["TurmaC"]["media"] = mediaAluno//Adiciona a nota do aluno
        }

    }
}
console.log('O aluno '+ totMediaAlunos['TurmaA']['nome']+' teve a média mais alta da turma A, com '+totMediaAlunos['TurmaA']['media']+'.' ) //Mostra no terminal o nome e a nota do aluno que possui a maior média da turma A
console.log('O aluno '+ totMediaAlunos['TurmaB']['nome']+' teve a média mais alta da turma B, com '+totMediaAlunos['TurmaB']['media']+'.' )//Mostra no terminal o nome e a nota do aluno que possui a maior média da turma B
console.log('O aluno '+ totMediaAlunos['TurmaC']['nome']+' teve a média mais alta da turma C, com '+totMediaAlunos['TurmaC']['media']+'.' )//Mostra no terminal o nome e a nota do aluno que possui a maior média da turma C